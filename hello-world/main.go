// Package main
package main

import (
	capsule "github.com/bots-garden/capsule-module-sdk"
)

var htmlPage string = `
<html>
  <head>
    <meta charset="utf-8">
    <title>Wasm is fantastic 😍</title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <style>
        .container { min-height: 100vh; display: flex; justify-content: center; align-items: center; text-align: center; }
        .title { font-family: "Source Sans Pro", "Helvetica Neue", Arial, sans-serif; display: block; font-weight: 300; font-size: 100px; color: #35495e; letter-spacing: 1px; }
        .subtitle { font-family: "Source Sans Pro", "Helvetica Neue", Arial, sans-serif; font-weight: 300; font-size: 42px; color: #526488; word-spacing: 5px; padding-bottom: 15px; }
        .links { padding-top: 15px; }
      </style>
  </head>

  <body>
    <section class="container">
      <div>
        <h1 class="title">👋 Hello World 🌍🤗🤖</h1>
        <h2 class="subtitle">Served with 💜 by Capsule 💊 [FaaS mode]</h2>
        <h2 class="subtitle">With 🦊 CI/CD components 📦</h2>
        <h2 class="subtitle">🍉🍓🍊🍋🥝</h2>
      </div>
    </section>
  </body>

</html>
`

func main() {

	capsule.SetHandleHTTP(func (param capsule.HTTPRequest) (capsule.HTTPResponse, error) {
		
		return capsule.HTTPResponse{
			TextBody: htmlPage,
			Headers: `{
				"Content-Type": "text/html; charset=utf-8",
				"Cache-Control": "no-cache",
				"X-Powered-By": "capsule-module-sdk"
			}`,
			StatusCode: 200,
		}, nil
	})
}
