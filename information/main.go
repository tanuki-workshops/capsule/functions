// Package main, this is a function demo
package main

import (
	"strconv"
	"strings"
	"github.com/bots-garden/capsule-module-sdk"
)

var counter int

func main() {
	counter = 0

	capsule.SetHandleHTTP(func(param capsule.HTTPRequest) (capsule.HTTPResponse, error) {

		// Counter for metrics
		counter++

		response := capsule.HTTPResponse{
			JSONBody:   `{"message": "this is an informative message"}`,
			Headers:    `{"Content-Type": "application/json; charset=utf-8"}`,
			StatusCode: 200,
		}
		return response, nil
	})
}

// OnHealthCheck function
//export OnHealthCheck
func OnHealthCheck() uint64 {
	response := capsule.HTTPResponse{
		JSONBody:   `{"message": "OK"}`,
		Headers:    `{"Content-Type": "application/json; charset=utf-8"}`,
		StatusCode: 200,
	}

	return capsule.Success([]byte(capsule.StringifyHTTPResponse(response)))
}

// OnMetrics function
//export OnMetrics
func OnMetrics() uint64 {

	// Generate OpenText Prometheus metric
	counterMetrics := []string{
		"# HELP call counter",
		"# TYPE call_counter counter",
		"call_counter " + strconv.Itoa(counter),}

	response := capsule.HTTPResponse{
		TextBody:   strings.Join(counterMetrics, "\n"),
		Headers:    `{"Content-Type": "text/plain; charset=utf-8"}`,
		StatusCode: 200,
	}
	return capsule.Success([]byte(capsule.StringifyHTTPResponse(response)))

}
